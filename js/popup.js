
var poputil = {};
var bgPage = chrome.extension.getBackgroundPage();

var optionsURL = chrome.extension.getURL("options.html");

bgPage.poller.getReviewsList('toReview');
bgPage.poller.getReviewsList('toSummarize');
bgPage.poller.getReviewsList('outForReview');
bgPage.poller.getReviewsList('completed/details');
/**
 * Render the list of reviews
 */
poputil.renderReviewsList = function() {

  	// Construct the iframe's HTML.
  	var toReviewHTML = [];
	var toSummarizeHTML = [];
	var outForReviewHTML = [];
	var completedHTML = [];

		
	for (var i = 0, review; review = bgPage.reviewsToReview[i]; ++i) {
		// If we have an arbitrary file, use generic file icon.


		toReviewHTML.push(
			'<li class="review" data-index="', i , '">',
			'<a href="', review.link ,'" target="_blank"><b>',
			review.id, '</b> - ',  review.name,'</a>',
			'<p>', review.description,'</p>',
			'</li>');
	}
	
	
	for (var i = 0, review; review = bgPage.reviewsToSummarize[i]; ++i) {
	// If we have an arbitrary file, use generic file icon.

	toSummarizeHTML.push(
		'<li class="review" data-index="', i , '">',
		'<a href="', review.link ,'" target="_blank"><b>',
		review.id, '</b> - ',  review.name,'</a>',
		'<p>', review.description,'</p>',
		'</li>');
	}
	
	
	for (var i = 0, review; review = bgPage.reviewsOutForReview[i]; ++i) {
		// If we have an arbitrary file, use generic file icon.

		outForReviewHTML.push(
			'<li class="review" data-index="', i , '">',
			'<a href="', review.link ,'" target="_blank"><b>',
			review.id, '</b> - ',  review.name,'</a>',
			'<p>', review.description,'</p>',
			'</li>');
	}
	
	for (var i = 0, review; review = bgPage.reviewsCompleted[i]; ++i) {
		// If we have an arbitrary file, use generic file icon.
		completedHTML.push(
			'<li class="review" data-index="', i , '">',
			'<a href="', review.link ,'" target="_blank"><b>',
			review.id, '</b> - ',  review.name,'</a>',
			'<p>', review.description,'</p>',
			'</li>');
	}

	if (toReviewHTML.length > 0){
	  	$('#output_toReview').html('<ul>' + toReviewHTML.join('') + '</ul>');
		$('#menu_toReview span').html('(' + bgPage.reviewsToReview.length.toString() + ')');
	}
	else 
		$('#output_toReview').html('You have no pending reviews');
	
	if (toSummarizeHTML.length > 0){
		$('#output_toSummarize').html('<ul>' + toSummarizeHTML.join('') + '</ul>');
		$('#menu_toSummarize span').html('(' + bgPage.reviewsToSummarize.length.toString() + ')');
	}
	else 
		$('#output_toSummarize').html('You have no pending reviews');
	
	if (outForReviewHTML.length > 0){
		$('#output_outForReview').html('<ul>' + outForReviewHTML.join('') + '</ul>');
		$('#menu_outForReview span').html('(' + bgPage.reviewsOutForReview.length.toString() + ')');
	}
	else 
		$('#output_outForReview').html('You have no pending reviews');
		
	if (completedHTML.length > 0){
		$('#output_completed').html('<ul>' + completedHTML.join('') + '</ul>');
		$('#menu_completed span').html('(' + bgPage.reviewsCompleted.length.toString() + ')');
	}
	else 
		$('#output_outForReviewoutput_completed').html('You have no pending reviews');
		
	toggleSection('toReview');

};

function toggleSection(section){
	$('.revs').hide();
	$('#menu li').removeClass('selected');
	$('#menu_' + section).addClass('selected');
	$('#output_' + section).show();
}

function refresh(){
  if (bgPage.notifications_count > 0) {
	    poputil.renderReviewsList();
	  }
	else{
		$('#output').html('You have no pending reviews');
	}
  return false;
}

$(function(){
	console.log(bgPage.crucible_auth);
	if ((bgPage.crucible_auth == '') || (bgPage.crucible_auth == null)){

		$('#popup').html('<p>You need to configure your <a id="option_link" href="#">server settings</a> first.</p>');
		$('#option_link').click(function(){
			chrome.tabs.create({
		    	url: "options.html"
			});
			return false;
		});
	}
	else {
		$('#toReview_toggle').click(function(){
			toggleSection('toReview');
			return false;	
		});
		$('#completed_toggle').click(function(){
			toggleSection('completed');
			return false;
		});
		$('#toSummarize_toggle').click(function(){
			toggleSection('toSummarize');
			return false;
		});
		$('#outForReview_toggle').click(function(){
			toggleSection('outForReview');
			return false;
		});
	
		$('#refresh').click(function(){
			return refresh();
		});
	
		if (bgPage.notifications_count > 0) {
		    poputil.renderReviewsList();
		  }
		else{
			$('#output').html('You have no pending reviews');
		}
	}
});